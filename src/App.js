import './App.css';
import {Switch, Route} from "react-router-dom";
import Menu from "./containers/Menu/Menu";
import Orders from "./containers/Orders/Orders";
import Layout from "./components/Layout/Layout";
import AddDish from "./components/AddDish/AddDish";
import EditDish from "./components/EditDish/EditDish";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Menu} />
            <Route path="/orders" exact component={Orders} />
            <Route path="/addDish" exact component={AddDish} />
            <Route path="/editDish/:id" exact component={EditDish} />
            <Route render={() => <h1>Not found</h1>}/>
        </Switch>
    </Layout>
);

export default App;
