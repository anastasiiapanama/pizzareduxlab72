import axios from "axios";

export const FETCH_DISHES_SUCCESS = 'FETCH_DISHES_SUCCESS';
export const FETCH_DISH_SUCCESS = 'FETCH_DISH_SUCCESS';
export const ADD_DISH = 'ADD_DISH';

export const fetchDishesSuccess = dishes => ({type: FETCH_DISHES_SUCCESS, dishes});
export const fetchDishSuccess = dish => ({type: FETCH_DISH_SUCCESS, dish});
export const addDishType = () => ({type: ADD_DISH});

export const fetchDishes = () => {
  return async dispatch => {
      const response = await axios.get('https://menuredux-ponamareva-default-rtdb.firebaseio.com/menu.json');

      const dishes = Object.keys(response.data).map(id => ({
          ...response.data[id],
          id
      }));

      dispatch(fetchDishesSuccess(dishes));
  };
};

export const deleteDish = id => {
    return async dispatch => {
        try {
            await axios.delete( 'https://menuredux-ponamareva-default-rtdb.firebaseio.com/menu/' + id + ".json");

            dispatch(fetchDishes());
        } catch (e) {
            console.log(e);
        }
    };
};

export const addDish = data => {
    return async dispatch => {
        try {
            await axios.post("https://menuredux-ponamareva-default-rtdb.firebaseio.com/menu.json", data);

            dispatch(addDishType());
            dispatch(fetchDishes());
        } catch (e) {
            console.log(e);
        }
    };
};

export const fetchDish = id => {
    return async dispatch => {
        try {
            const response = await axios.get('https://menuredux-ponamareva-default-rtdb.firebaseio.com/menu/' + id + '.json');

            dispatch(fetchDishSuccess(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};

export const editDish = (id, content, history) => {

    return async dispatch => {
        try {
            await axios.put(`https://menuredux-ponamareva-default-rtdb.firebaseio.com/menu/${id}.json`, content);
            history.push('/')
        } catch (e) {
            console.log(e);
        }
    };
};