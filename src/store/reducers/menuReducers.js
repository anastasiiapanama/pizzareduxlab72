import {ADD_DISH, FETCH_DISH_SUCCESS, FETCH_DISHES_SUCCESS} from "../actions/menuActions";

const initialState = {
    dishes: [],
    oneDish: {},
    ordered: false,
};

const menuReducers = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DISHES_SUCCESS:
            return {...state, dishes: action.dishes, ordered: true};
        case FETCH_DISH_SUCCESS:
            return {...state, oneDish: action.dish};
        case ADD_DISH:
            return {...state};
        default:
            return state;
    }
};

export default menuReducers;