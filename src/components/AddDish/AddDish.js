import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import {addDish} from "../../store/actions/menuActions";

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2)
    }
}));

const AddDish = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [content, setContent] = useState({
        title: '',
        price: '',
        image: ''
    });

    const changeContent = event => {
        const {name, value} = event.target;

        setContent(prev =>({
            ...prev,
            [name]: value
        }));
    };

    const submitTasks = event => {
        event.preventDefault();

        dispatch(addDish(content))
    };

    return (
        <Paper className={classes.paper}>
            <form onSubmit={submitTasks}>
                <Grid container direction="column" spacing={3}>
                    <Grid item>
                        <Typography variant="h5" align="center">Add new Dish</Typography>
                    </Grid>
                    <Grid item>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="title"
                            label="Title"
                            value={content.title}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="price"
                            label="Price"
                            value={content.price}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="image"
                            label="Image"
                            value={content.image}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <Button type="submit" variant="contained" color="primary">Add</Button>
                    </Grid>
                </Grid>
            </form>
        </Paper>
    );
};

export default AddDish;