import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {editDish, fetchDish} from "../../store/actions/menuActions";

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2)
    }
}));

const EditDish = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const oneMenuItem = useSelector(state => state.dishes.oneDish);

    const [oneDish, setOneDish] = useState({});

    useEffect(() => {
        dispatch(fetchDish(props.match.params.id));
    }, [dispatch, props.match.params.id]);

    useEffect(() => {
        setOneDish(oneMenuItem);
    }, [oneMenuItem]);


    const changeContent = event => {
        const {name, value} = event.target;

        setOneDish(prev =>({
            ...prev,
            [name]: value
        }));
    };

    const submitTasks = event => {
        event.preventDefault();

        dispatch(editDish(props.match.params.id, oneDish, props.history));
    };

    return (
        <Paper className={classes.paper}>
            <form onSubmit={submitTasks}>
                <Grid container direction="column" spacing={3}>
                    <Grid item>
                        <Typography variant="h5" align="center">Add new Dish</Typography>
                    </Grid>
                    <Grid item>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="title"
                            value={oneDish.title}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="price"
                            value={oneDish.price}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <TextField
                            variant="outlined"
                            fullwidth
                            type="text"
                            name="image"
                            value={oneDish.image}
                            required
                            onChange={changeContent}
                        />
                    </Grid>
                    <Grid item>
                        <Button type="submit" variant="contained" color="primary">Save</Button>
                    </Grid>
                </Grid>
            </form>
        </Paper>
    );
};

export default EditDish;