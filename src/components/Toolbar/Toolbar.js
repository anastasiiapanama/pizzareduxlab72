import React from 'react';
import NavigationItems from "../Navigation/NavigationItems/NavigationItems";
import './Toolbar.css';

const Toolbar = () => {
    return (
        <header className="Toolbar">
            <div className="Toolbar-logo">Turtle Pizza Admin</div>
            <nav><NavigationItems/></nav>
        </header>
    );
};

export default Toolbar;