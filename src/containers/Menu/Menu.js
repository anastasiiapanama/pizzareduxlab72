import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteDish, fetchDishes} from "../../store/actions/menuActions";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from "@material-ui/core/Typography";
import Dish from "../../components/Dish/Dish";
import {NavLink} from "react-router-dom";

const Menu = () => {
    const dispatch = useDispatch();
    const dishes = useSelector(state => state.dishes.dishes);

    useEffect(() => {
        dispatch(fetchDishes());
    }, [dispatch]);

    const removeDishHandler = (e, id) => {
        e.preventDefault();

        dispatch(deleteDish(id));
    };

    return(
        <>
            <CssBaseline/>
            <Container>
                <Grid container spacing={4} direction="column">
                    <Grid container item direction="column" alignItems="center">
                        <Grid item xs={6} spacing={3}>
                            <Typography variant="h4">Menu</Typography>
                        </Grid>
                        <Grid item xs={6} justify="space-around">
                            <NavLink to="/addDish">Add new Dish</NavLink>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Paper>
                            {dishes.map(dish => (
                                <Dish
                                    id={dish.id}
                                    key={dish.id}
                                    title={dish.title}
                                    price={dish.price}
                                    image={dish.image}
                                    remove={e => removeDishHandler(e, dish.id)}
                                />
                            ))}
                        </Paper>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
};

export default Menu;